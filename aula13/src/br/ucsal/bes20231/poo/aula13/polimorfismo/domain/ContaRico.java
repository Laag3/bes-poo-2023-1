package br.ucsal.bes20231.poo.aula13.polimorfismo.domain;

import br.ucsal.bes20231.poo.aula13.polimorfismo.exception.NegocioException;

public class ContaRico extends ContaCorrente {

	public ContaRico(Integer numero, String nomeCorrentista) {
		super(numero, nomeCorrentista);
	}

	public static void fazerAlgo() {
		System.out.println("fiz algo na ContaRico");
	}

	@Override
	public void sacar(Double valor) throws NegocioException {
		saldo -= valor;
	}

}
