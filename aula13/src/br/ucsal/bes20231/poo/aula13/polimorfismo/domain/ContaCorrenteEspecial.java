package br.ucsal.bes20231.poo.aula13.polimorfismo.domain;

import br.ucsal.bes20231.poo.aula13.polimorfismo.exception.NegocioException;

public class ContaCorrenteEspecial extends ContaCorrente {

	private Double limiteCredito;

	public ContaCorrenteEspecial(Integer numero, String nomeCorrentista, Double limiteCredito) {
		super(numero, nomeCorrentista);
		this.limiteCredito = limiteCredito;
	}

	public static void fazerAlgo() {
		System.out.println("fiz algo na ContaCorrenteEspecial");
	}

	@Override
	public void sacar(Double valor) throws NegocioException {
		verificarSituacaoConta();
		if (valor > saldo + limiteCredito) {
			throw new NegocioException("Saldo, incluindo o limite de crédito, é insuficiente.");
		}
		saldo -= valor;
	}
	
	public Double getLimiteCredito() {
		return limiteCredito;
	}

	public void atualizarLimiteCredito(Double limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

}
