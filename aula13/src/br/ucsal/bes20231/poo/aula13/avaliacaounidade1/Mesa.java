package br.ucsal.bes20231.poo.aula13.avaliacaounidade1;

public class Mesa {

	private String codigo;

	private int qtdCadeiras;

	public Mesa(String codigo, int qtdCadeiras) {
		super();
		this.codigo = codigo;
		this.qtdCadeiras = qtdCadeiras;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getQtdCadeiras() {
		return qtdCadeiras;
	}

	public void setQtdCadeiras(int qtdCadeiras) {
		this.qtdCadeiras = qtdCadeiras;
	}

}
