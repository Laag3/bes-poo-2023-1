package br.ucsal.bes20231.poo.aula13.avaliacaounidade1;

import java.util.ArrayList;
import java.util.List;

public class Comanda {

	private int numero;

	private List<ItemConsumido> itensConsumidos = new ArrayList<>();

	// private List<Item> itens = new ArrayList<>();
	// private List<Integer> qtdsItens;

	// Não é interessante definir o tipo como ArrayList, conforme já discutido em
	// sala.
	// private ArrayList<Item> itens = new ArrayList<>();
	// private ArrayList<Integer> qtdsItens;

	// private Item[] itens;
	// private int[] qtdsItens;

	private Garcom garcom;

	private Mesa mesa;

	private boolean aberta;

	private int qtdClientes;

	public Comanda(int numero, Garcom garcom, Mesa mesa, boolean aberta, int qtdClientes) {
		super();
		this.numero = numero;
		this.garcom = garcom;
		this.mesa = mesa;
		this.aberta = aberta;
		this.qtdClientes = qtdClientes;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public List<ItemConsumido> getItensConsumidos() {
		return new ArrayList<>(itensConsumidos);
	}

	public void addItemConsumido(Item item, int qtd) {
		ItemConsumido itemConsumido = new ItemConsumido(item, qtd);
		itensConsumidos.add(itemConsumido);
	}

	public Garcom getGarcom() {
		return garcom;
	}

	public void setGarcom(Garcom garcom) {
		this.garcom = garcom;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	public boolean isAberta() {
		return aberta;
	}

	public void setAberta(boolean aberta) {
		this.aberta = aberta;
	}

	public int getQtdClientes() {
		return qtdClientes;
	}

	public void setQtdClientes(int qtdClientes) {
		this.qtdClientes = qtdClientes;
	}

}
