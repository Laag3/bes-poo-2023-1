package br.ucsal.bes20231.poo.aula13.avaliacaounidade1;

import java.util.ArrayList;
import java.util.List;

public class Garcom {

	private int matricula;

	private String nome;

	private List<String> telefones;

	public Garcom(int matricula, String nome, List<String> telefones) {
		super();
		this.matricula = matricula;
		this.nome = nome;
		this.telefones = telefones;
	}

	public int getMatricula() {
		return matricula;
	}

	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<String> getTelefones() {
		return new ArrayList<>(telefones);
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

}
