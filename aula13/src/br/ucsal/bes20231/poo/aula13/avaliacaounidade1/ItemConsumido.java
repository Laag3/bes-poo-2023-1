package br.ucsal.bes20231.poo.aula13.avaliacaounidade1;

public class ItemConsumido {

	private Item item;

	private int qtd;

	public ItemConsumido(Item item, int qtd) {
		super();
		this.item = item;
		this.qtd = qtd;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public int getQtd() {
		return qtd;
	}

	public void setQtd(int qtd) {
		this.qtd = qtd;
	}

}
