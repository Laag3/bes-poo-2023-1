package br.ucsal.bes20231.poo.aula12.exception;

public class ContaBloqueadaException extends Exception {

	private static final long serialVersionUID = 1L;

	public ContaBloqueadaException() {
		super("Conta bloqueada.");
	}
}
