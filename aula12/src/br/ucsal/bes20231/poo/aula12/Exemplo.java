package br.ucsal.bes20231.poo.aula12;

import br.ucsal.bes20231.poo.aula12.domain.ContaCorrente;
import br.ucsal.bes20231.poo.aula12.exception.ContaBloqueadaException;
import br.ucsal.bes20231.poo.aula12.exception.NegocioException;
import br.ucsal.bes20231.poo.aula12.exception.SaldoInsuficienteException;

public class Exemplo {

	public static void main(String[] args) {

		try {
			executarLogicaExemplo();
		} catch (Throwable e) {
			System.out.println("Ocorreu um erro inesperado. Por favor, tente outra vez, mais tarde!");
			// Faço um log do problema!
			// Passo um email pra o programador, dizendo que deu um problema não previsto!
		}

	}

	private static void executarLogicaExemplo() {
		ContaCorrente contaCorrente1 = new ContaCorrente(123, "claudio");

		contaCorrente1.depositar(1000.);

//		try {
//
//			contaCorrente1.sacar(1200.);
//
//			System.out.println("saque realizado com sucesso!");
//
//			System.out.println("saldo da conta corrente de " + contaCorrente1.getNomeCorrentista() + " = "
//					+ contaCorrente1.consultarSaldo());
//
//		} catch (ContaBloqueadaException e) {
//
//			System.out.println("Calma... o dinheiro já vai sair...");
//			// mandar email pra polícia!
//
//		} catch (SaldoInsuficienteException e) {
//
//			System.out.println("Erro ao realizar o saque: " + e.getMessage());
//
//		}

		try {

			contaCorrente1.sacar(1200.);

			System.out.println("saque realizado com sucesso!");

			System.out.println("saldo da conta corrente de " + contaCorrente1.getNomeCorrentista() + " = "
					+ contaCorrente1.consultarSaldo());

		} catch (NegocioException e) {

//			Integer n = null;
//			n++;

			System.out.println("Erro ao realizar o saque: " + e.getMessage());
			
		} finally {

			System.out.println("Pode chover canivete, que esse código vai rodar :)");

		}

		System.out.println("Aqui vai \"quase sempre\" ser executado!");
	}

}
