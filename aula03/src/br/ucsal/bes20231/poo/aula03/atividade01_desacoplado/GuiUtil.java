package br.ucsal.bes20231.poo.aula03.atividade01_desacoplado;

import javax.swing.JOptionPane;

public class GuiUtil implements Interface {

	public int obterInteiro(String mensagem) {
		String inteiroString;
		inteiroString = JOptionPane.showInputDialog(mensagem);
		return Integer.parseInt(inteiroString);
	}

	public void exibirMensagem(String mensagem) {
		JOptionPane.showMessageDialog(null, mensagem);
	}

}
