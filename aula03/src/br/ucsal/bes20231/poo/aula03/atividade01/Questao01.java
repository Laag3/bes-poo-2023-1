package br.ucsal.bes20231.poo.aula03.atividade01;

import java.util.Scanner;

/**
 * Suponha que o conceito de um aluno seja determinado em função da sua nota.
 * Suponha, também, que esta nota seja um valor inteiro na faixa de 0 a 100
 * (intervalo fechado), conforme a seguinte faixa:
 * 
 * Nota Conceito 0 a 49 Insuficiente 50 a 64 Regular 65 a 84 Bom 85 a 100 Ótimo
 * 
 * Crie um programa em Java que leia a nota de um aluno e apresente o conceito
 * do mesmo. Não é necessário tratar valores fora da faixa.
 * 
 * @author antoniocp
 *
 */
public class Questao01 {

	public static void main(String[] args) {
		obterNotaExibirConceito();
		br.ucsal.bes20231.poo.aula03.atividade01.util.Scanner meuScanner;
	}

	private static void obterNotaExibirConceito() {
		int nota;
		String conceito;
		nota = obterNota();
		conceito = calcularConceito(nota);
		exibirConceito(conceito);
	}

	private static void exibirConceito(String conceito) {
		System.out.println("Conceito=" + conceito);
	}

	private static String calcularConceito(int nota) {
		String conceito;
		if (nota <= 49) {
			conceito = "Insuficiente";
		} else if (nota <= 64) {
			conceito = "Regular";
		} else if (nota <= 84) {
			conceito = "Bom";
		} else {
			conceito = "Ótimo";
		}
		return conceito;
	}

	private static int obterNota() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe a nota (0 a 100), intervalo fechado):");
		return scanner.nextInt();
	}

}
