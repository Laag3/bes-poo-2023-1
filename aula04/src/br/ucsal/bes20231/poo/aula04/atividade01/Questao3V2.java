package br.ucsal.bes20231.poo.aula04.atividade01;

import java.util.Scanner;

public class Questao3V2 {

	private static final int QTD_NUMEROS = 3;

	public static void main(String[] args) {
		calcularMaiorMenorMedia();
	}

	private static void calcularMaiorMenorMedia() {
		int[] vet = new int[QTD_NUMEROS];
		int maior;
		int menor;
		float media;

		obterNumeros(vet);
		maior = calcularMaior(vet);
		menor = calcularMenor(vet);
		media = calcularMedia(vet);

		exibirResultado("maior", maior);
		exibirResultado("menor", menor);
		exibirResultado("medio", media);
	}

	private static void exibirResultado(String nome, int valor) {
		System.out.println(nome + " = " + valor);
	}

	private static void exibirResultado(String nome, float valor) {
		System.out.println(nome + " = " + valor);
	}

	private static float calcularMedia(int[] vet) {
		int soma = 0;
		for (int i = 0; i < QTD_NUMEROS; i++) {
			soma += vet[i];
		}
		return soma / (float) QTD_NUMEROS;
	}

	private static int calcularMaior(int[] vet) {
		int maior = 0;
		for (int i = 0; i < QTD_NUMEROS; i++) {
			if (i == 0) {
				maior = vet[i];
			} else {
				if (vet[i] > maior) {
					maior = vet[i];
				}
			}
		}
		return maior;
	}

	private static int calcularMenor(int[] vet) {
		int menor = 0;
		for (int i = 0; i < QTD_NUMEROS; i++) {
			if (i == 0) {
				menor = vet[i];
			} else {
				if (vet[i] < menor) {
					menor = vet[i];
				}
			}
		}
		return menor;
	}

	private static void obterNumeros(int[] vet) {
		Scanner scanner = new Scanner(System.in);

		for (int i = 0; i < QTD_NUMEROS; i++) {
			System.out.print("Informe o numero " + (i + 1) + ":");
			vet[i] = scanner.nextInt();
		}
	}

}
