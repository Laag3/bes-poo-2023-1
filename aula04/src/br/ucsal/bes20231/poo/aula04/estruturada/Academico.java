package br.ucsal.bes20231.poo.aula04.estruturada;

import java.time.LocalDate;

public class Academico {

	int matriculaAluno;

	String nomeAluno;

	LocalDate dataIngressoAluno;

	LocalDate dataNascimentoAluno;

	String cpfProfessor;

	String nomeProfessor;

	LocalDate dataAdmissaoProfessor;

	String titulacaoProfessor;

	void lecionarTurma() {
	}

	void entrarFerias() {
	}

	void registrarConteudoAula() {
	}

	void trancarCurso() {
	}

	void matricular() {
	}

	void cancelarCurso() {
	}
}
