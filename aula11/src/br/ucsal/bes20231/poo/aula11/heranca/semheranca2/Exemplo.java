package br.ucsal.bes20231.poo.aula11.heranca.semheranca2;

import java.util.ArrayList;
import java.util.List;

public class Exemplo {

	public static void main(String[] args) {
		
		List<Pessoa> clientes = new ArrayList<>();
		
		Pessoa pessoaFisica = new Pessoa();
		Pessoa pessoaJuridica = new Pessoa();

		pessoaFisica.cpf = "123";
		pessoaFisica.cnpj = "567";
		
		clientes.add(pessoaFisica);
		clientes.add(pessoaJuridica);
		//clientes.add(123);
	}
	
}
