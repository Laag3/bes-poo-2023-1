package br.ucsal.bes20231.poo.aula11.heranca.semheranca1;

import java.util.ArrayList;
import java.util.List;

public class Exemplo {

	public static void main(String[] args) {
		
		List<Object> clientes = new ArrayList<>();
		
		PessoaFisica pessoaFisica = new PessoaFisica();
		PessoaJuridica pessoaJuridica = new PessoaJuridica();

		clientes.add(pessoaFisica);
		clientes.add(pessoaJuridica);
		clientes.add(123);
		
		enviarEmailCliente(pessoaFisica);
		enviarEmailCliente(pessoaJuridica);
	}
	
	private static void enviarEmailCliente(Object cliente) {
		// aqui a lógica para enviar email
		// String email = cliente.email;
	}
	
}
