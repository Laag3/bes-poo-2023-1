package br.ucsal.bes20231.poo.aula07;

import java.util.Scanner;

public class ExemploNomesSemLista {

	private static final int QTD_NOMES = 5;
	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		String[] nomes = new String[QTD_NOMES];

		obterNomes(nomes);

		exibirTodos(nomes);

		String nomeRemover = obterNomeRemover();
		removerNome(nomes, nomeRemover);

		exibirNomeAposRemocao(nomes);

	}

	private static void exibirTodos(String[] nomes) {
		System.out.println("Todos os nomes da lista:");
		for (int i = 0; i < nomes.length; i++) {
			// AQUI eles manipulam CADA UM dos itens de um conjunto
			System.out.println(nomes[i]);
		}
		for (int i = 0; i < nomes.length; i++) {
			String nome = nomes[i];
			// AQUI eles manipulam CADA UM dos itens de um conjunto
			System.out.println(nome);
		}
		for (String nome : nomes) {
			// AQUI eles manipulam CADA UM dos itens de um conjunto
			System.out.println(nome);
		}
	}

	private static void exibirNomeAposRemocao(String[] nomes) {
		System.out.println("Lista sem o nome removido:");
		for (int i = 0; i < nomes.length - 1; i++) {
			System.out.println(nomes[i]);
		}
	}

	private static void removerNome(String[] nomes, String nomeRemover) {
		for (int i = encontrarNome(nomes, nomeRemover); i < nomes.length - 1; i++) {
			nomes[i] = nomes[i + 1];
		}
	}

	private static String obterNomeRemover() {
		System.out.println("Informe um nome a ser removido:");
		String nomeRemover = scanner.nextLine();
		return nomeRemover;
	}

	private static void obterNomes(String[] nomes) {
		System.out.println("Informe " + QTD_NOMES + ":");
		for (int i = 0; i < nomes.length; i++) {
			nomes[i] = scanner.nextLine();
		}
	}

	private static int encontrarNome(String[] nomes, String nomePesquisa) {
		for (int i = 0; i < nomes.length; i++) {
			if (nomePesquisa.equals(nomes[i])) {
				return i;
			}
		}
		return -1;
	}

}
