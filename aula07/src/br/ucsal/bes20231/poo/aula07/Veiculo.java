package br.ucsal.bes20231.poo.aula07;

import java.util.Objects;

public class Veiculo {

	private String placa;

	private int anoFabricacao;

	private double valor;

	private Pessoa proprietario;

	public Veiculo() {
	}

	public Veiculo(String placa) {
		setPlaca(placa);
	}

	public Veiculo(String placa, int anoFabricacao) {
		this(placa);
		this.anoFabricacao = anoFabricacao;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa.toUpperCase();
	}

	public int getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(int anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Pessoa getProprietario() {
		return proprietario;
	}

	public void setProprietario(Pessoa proprietario) {
		this.proprietario = proprietario;
	}

	@Override
	public int hashCode() {
		return Objects.hash(anoFabricacao, placa, proprietario, valor);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Veiculo other = (Veiculo) obj;
		return anoFabricacao == other.anoFabricacao && Objects.equals(placa, other.placa)
				&& Objects.equals(proprietario, other.proprietario)
				&& Double.doubleToLongBits(valor) == Double.doubleToLongBits(other.valor);
	}

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", anoFabricacao=" + anoFabricacao + ", valor=" + valor + ", proprietario="
				+ proprietario + "]";
	}

}
