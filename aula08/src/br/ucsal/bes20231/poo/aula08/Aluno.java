package br.ucsal.bes20231.poo.aula08;

public class Aluno {

	private static int contador = 0;

	private int matricula;

	private String nome;

	public Aluno(String nome) {
		definirMatricula();
		this.nome = nome;
	}

	public int getMatricula() {
		return matricula;
	}

	public static int getContador() {
		return Aluno.contador;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	private void definirMatricula() {
		Aluno.contador ++;
		this.matricula = Aluno.contador;
	}
	

}
